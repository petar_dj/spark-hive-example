name := "spark-hive-example"

version := "0.1"

scalaVersion := "2.11.11"

val sparkVersion = "2.4.3"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-avro" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-hive" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-streaming" % sparkVersion % "compile",
  //  ("org.apache.hadoop" % "hadoop-common" % "2.4.0")
  //    .exclude("hadoop-common", "hadoop-annotations")
)


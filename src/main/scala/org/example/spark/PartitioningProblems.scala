package org.example.spark

import org.apache.spark.sql.SparkSession
import org.apache.spark.util.SizeEstimator

object PartitioningProblems {

  val spark = SparkSession.builder()
    .appName("Partitioning Problems")
    .master("local[*]")
    .getOrCreate()

  def processNumbers(nPartitions: Int) = {
    val numbers = spark.range(100000000) // ~800MB
    val repartitionedNumbers = numbers.repartition(nPartitions)
    repartitionedNumbers.cache()
    repartitionedNumbers.count()


    repartitionedNumbers.selectExpr("sum(id)").show()
  }

  // 1 - use size estimator
  def dfSizeEstimator() = {
    val numbers = spark.range(100000)
    println(SizeEstimator.estimate(numbers))
    numbers.cache()
    numbers.count()
  }

  // 2 - use query plan
  def estimateWithQueryPlan() = {
    val numbers = spark.range(100000)
    println(numbers.queryExecution.optimizedPlan.stats.sizeInBytes)
  }

  def estimateRDD() = {
    val numbers = spark.sparkContext.parallelize(1 to 100000)
    numbers.cache().count()
  }

  def main(args: Array[String]): Unit = {
    processNumbers(2) // 400MB / partition
    processNumbers(20) // 40MB / partition
    processNumbers(200) // 4MB / partition
    processNumbers(2000) // 400KB / partition
    processNumbers(20000) // 40KB / partition

     dfSizeEstimator()
     estimateWithQueryPlan()
     estimateRDD()

  }

}

package org.example.spark

import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object PrePartitioning extends App {

  val spark = SparkSession.builder()
    .appName("Pre-partitioning Petar")
    .getOrCreate()

  import spark.implicits._

  // deactivate broadcast joins
  spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

  def addColumns[T](df: Dataset[T], n: Int): DataFrame = {
    val newColumns = (1 to n).map(i => s"id * $i as newCol$i")
    df.selectExpr(("id" +: newColumns): _*)
  }

  // don't touch this
  val initialTable = spark.range(1, 10000000).repartition(10) // RoundRobinPartitioning(10)
  val narrowTable = spark.range(1, 5000000).repartition(7) // RoundRobinPartitioning(7)

  // scenario 1
  val wideTable = addColumns(initialTable, 30)
  val join1 = wideTable.join(narrowTable, "id")

  join1.show()

  // scenario 2
  val altNarrow = narrowTable.repartition($"id") // use a HashPartitioner
  val altInitial = initialTable.repartition($"id")
  // join on co-partitioned DFs
  val join2 = altInitial.join(altNarrow, "id")
  val result2 = addColumns(join2, 30)
  result2.show()

  val enhanceColumnsFirst = addColumns(initialTable, 30)
  val repartitionedNarrow = narrowTable.repartition($"id")
  val repartitionedEnhanced = enhanceColumnsFirst.repartition($"id")
  val result3 = enhanceColumnsFirst.join(repartitionedNarrow, "id")

  result3.show()
  initialTable.join(narrowTable.repartition(10), "id").show()

}

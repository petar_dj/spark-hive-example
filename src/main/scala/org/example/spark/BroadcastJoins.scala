package org.example.spark

import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

object BroadcastJoins {

  val spark = SparkSession.builder()
    .appName("Broadcast Joins")
    .master("local")
    .getOrCreate()

  val sc = spark.sparkContext

  val rows = sc.parallelize(List(
    Row(0, "zero"),
    Row(1, "first"),
    Row(2, "second"),
    Row(3, "third")
  ))

  val rowsSchema = StructType(Array(
    StructField("id", IntegerType),
    StructField("order", StringType)
  ))


  val lookupTable: DataFrame = spark.createDataFrame(rows, rowsSchema)


  val table = spark.range(1, 100000000) // column is "id"


  val joined = table.join(lookupTable, "id")

  val joinedSmart = table.join(broadcast(lookupTable), "id")
  joinedSmart.explain()
  joinedSmart.show()

  val bigTable = spark.range(1, 100000000)
  val smallTable = spark.range(1, 10000) // size estimated by Spark - auto-broadcast
  val joinedNumbers = smallTable.join(bigTable, "id")

  spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

  joinedNumbers.explain()

  def main(args: Array[String]): Unit = {
    Thread.sleep(1000000)
  }
}

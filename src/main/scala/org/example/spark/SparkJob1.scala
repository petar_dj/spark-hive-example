package org.example.spark

import org.apache.spark.sql.SparkSession

object SparkJob1 extends App {
  val spark = SparkSession.builder()
    .appName("Petar's job Spark anatomy")
    .enableHiveSupport()
    .getOrCreate()
  val sc = spark.sparkContext
  val rdd1 = sc.parallelize(1 to 1000000)
  rdd1.count
  rdd1.map(_ * 2).count
  rdd1.repartition(23).count
}

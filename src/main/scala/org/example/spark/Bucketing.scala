package org.example.spark

import org.apache.spark.sql.SparkSession

object Bucketing extends App {

  val spark = SparkSession.builder()
    .appName("Bucketing Petar")
    .getOrCreate()

  import spark.implicits._


  spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

  val large = spark.range(1000000).selectExpr("id * 5 as id").repartition(10)
  val small = spark.range(10000).selectExpr("id * 3 as id").repartition(3)

  val joined = large.join(small, "id")

  joined.show()

  large.write
    .bucketBy(4, "id")
    .sortBy("id")
    .mode("overwrite")
    .saveAsTable("bucketed_large")

  small.write
    .bucketBy(4, "id")
    .sortBy("id")
    .mode("overwrite")
    .saveAsTable("bucketed_small")

  spark.sql("use default")
  val bucketedLarge = spark.table("bucketed_large")
  val bucketedSmall = spark.table("bucketed_small")
  val bucketedJoin = bucketedLarge.join(bucketedSmall, "id")

  bucketedJoin.show()

  val flightsDF = spark.read
    .option("inferSchema", "true")
    .json(args(0))
    .repartition(2)

  val mostDelayed = flightsDF
    .filter("origin = 'DEN' and arrdelay > 1")
    .groupBy("origin", "dest", "carrier")
    .avg("arrdelay")
    .orderBy($"avg(arrdelay)".desc_nulls_last)

  val the10 = bucketedLarge.filter($"id" === 10)
  the10.show()

}

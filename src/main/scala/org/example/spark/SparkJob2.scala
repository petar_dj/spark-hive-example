package org.example.spark

import org.apache.spark.sql.SparkSession

object SparkJob2 extends App {
  val spark = SparkSession.builder()
    .appName("Petar's job Spark anatomy")
    .getOrCreate()

  val sc = spark.sparkContext

  val employees = sc.textFile(args(0))

  val empTokens = employees.map(line => line.split(","))

  val empDetails = empTokens.map(tokens => (tokens(4), tokens(7)))

  val empGroups = empDetails.groupByKey(2)

  val avgSalaries = empGroups.mapValues(salaries => salaries.map(_.toInt).sum / salaries.size)

  avgSalaries
    .collect()
    .foreach(println)
}

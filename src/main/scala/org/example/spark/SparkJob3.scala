package org.example.spark

import org.apache.spark.sql.SparkSession

object SparkJob3 extends App {

  val spark = SparkSession.builder()
    .appName("Petar's job Spark anatomy")
    .getOrCreate()

  import spark.implicits._

  val sc = spark.sparkContext

  val numbers = 1 to 1000000000
  val rdd = sc.parallelize(1 to 1000000000)
  rdd.count()

  val df = rdd.toDF("id")
  df.count()

  val dfCount = df.selectExpr("count(*)")
  val ds = spark.range(1, 1000000000)
  ds.count()

  val dsCount = ds.selectExpr("count(*)")

  ds.toDF("value").count()
  ds.rdd.count()

  val rddTimes5 = rdd.map(_ * 5)
  rddTimes5.count()

  val dfTimes5 = df.selectExpr("id * 5 as id1")
  val dfTimes5Count = dfTimes5.selectExpr("count(*)")

  val dsTimes5 = ds.map(_ * 5)
  val dsTimes5Count = dsTimes5.selectExpr("count(*)")

}

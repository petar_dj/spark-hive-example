package org.example.spark
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel

object Checkpointing extends App {

  val spark = SparkSession.builder()
    .appName("Checkpointing")
    .master("local")
    .getOrCreate()

  val sc = spark.sparkContext

  def demoCheckpoint() = {
    val flightsDF = spark.read
      .option("inferSchema", "true")
      .json(args(0))

    val orderedFlights = flightsDF.orderBy("dist")

    sc.setCheckpointDir(args(1))

    val checkpointedFlights = orderedFlights.checkpoint()


    checkpointedFlights.show()
  }

  def cachingJobRDD() = {
    val numbers = sc.parallelize(1 to 10000000)
    val descNumbers = numbers.sortBy(-_).persist(StorageLevel.DISK_ONLY)
    descNumbers.sum()
    descNumbers.sum()
  }

  def checkpointingJobRDD() = {
    sc.setCheckpointDir(args(1))
    val numbers = sc.parallelize(1 to 10000000)
    val descNumbers = numbers.sortBy(-_)
    descNumbers.checkpoint()
    descNumbers.sum()
    descNumbers.sum()
  }

  def cachingJobDF() = {
    val flightsDF = spark.read
      .option("inferSchema", "true")
      .json(args(0))

    val orderedFlights = flightsDF.orderBy("dist")
    orderedFlights.persist(StorageLevel.DISK_ONLY)
    orderedFlights.count()
    orderedFlights.count()
  }

  def checkpointingJobDF() = {
    sc.setCheckpointDir(args(1))
    val flightsDF = spark.read
      .option("inferSchema", "true")
      .json(args(0))

    val orderedFlights = flightsDF.orderBy("dist")
    val checkpointedFlights = orderedFlights.checkpoint()
    checkpointedFlights.count()
    checkpointedFlights.count()
  }

  demoCheckpoint()
  cachingJobRDD()
  checkpointingJobRDD()
  cachingJobDF()
  checkpointingJobDF()

}

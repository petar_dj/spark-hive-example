package org.example.spark

import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel

object Caching extends App {

  val spark = SparkSession.builder()
    .appName("Caching")
    .config("spark.memory.offHeap.enabled", "true")
    .config("spark.memory.offHeap.size", 10000000)
    .master("local")
    .getOrCreate()

  val flightsDF = spark.read
    .option("inferSchema", "true")
    .json(args(0))

  flightsDF.count()

  val orderedFlightsDF = flightsDF.orderBy("dist")

  orderedFlightsDF.persist(
    StorageLevel.OFF_HEAP
  )

  orderedFlightsDF.count()
  orderedFlightsDF.count()


  orderedFlightsDF.unpersist()

  orderedFlightsDF.createOrReplaceTempView("orderedFlights")
  spark.catalog.cacheTable("orderedFlights")
  orderedFlightsDF.count()

  val flightsRDD = orderedFlightsDF.rdd
  flightsRDD.persist(StorageLevel.MEMORY_ONLY_SER)
  flightsRDD.count()

}

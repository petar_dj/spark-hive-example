package org.example.spark

import org.apache.spark.sql.SparkSession

object SparkHiveExample extends App {
  val readLocation = "hdfs://nameservice1/user/hdfs/hive_playground/Bane/weather"
  val tableName = "petar.weather_orc"
  val spark = SparkSession.builder()
    .appName("Write DF to Hive")
    .enableHiveSupport()
    .getOrCreate()

  val df = spark.read
    .option("header", "true")
    .parquet(readLocation)

  df
    .write
    .mode("append")
    .format("hive")
    .saveAsTable(tableName)


}

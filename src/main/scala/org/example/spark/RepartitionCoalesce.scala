package org.example.spark

import org.apache.spark.sql.SparkSession

object RepartitionCoalesce {

  val spark = SparkSession.builder()
    .appName("Repartition and Coalesce")
    .master("local[*]")
    .getOrCreate()

  val sc = spark.sparkContext

  val numbers = sc.parallelize(1 to 10000000)
  println(numbers.partitions.length)


  val repartitionedNumbers = numbers.repartition(2)
  repartitionedNumbers.count()

  val coalescedNumbers = numbers.coalesce(2)
  coalescedNumbers.count()

  val forcedShuffledNumbers = numbers.coalesce(2, true)

}
